import React, { Component } from "react";
import { glassArr } from "./dataGlasses";

export default class RenderGlasses extends Component {
  state = {
    glassTest: glassArr[0],
  };
  handleChangeGlass = (glass) => {
    this.setState({ glassTest: glass });
  };
  renderGLassList = () => {
    let glassList = glassArr.map((item) => {
      return (
        <div className="col-4">
          <img
            onClick={() => {
              this.handleChangeGlass(item);
            }}
            className="p-2"
            style={{ width: "10rem" }}
            src={item.url}
            alt=""
          />
        </div>
      );
    });
    return glassList;
  };

  render() {
    return (
      <div
        className="text-center"
        style={{
          backgroundImage: `url("./glassesImage/background.jpg")`,
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          height: "100vh",
        }}
      >
        <div>
          <img src="./glassesImage/model.jpg" />
          <div style={{ position: "absolute", top: "150px", left: "42.5%" }}>
            <img
              style={{ width: "18rem" }}
              src={this.state.glassTest.url}
              alt=""
            />
          </div>
        </div>
        <div className="row">{this.renderGLassList()}</div>
      </div>
    );
  }
}
